package org.owasp.webgoat.server;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
//test
@Configuration
@ComponentScan("org.owasp.webgoat.server")
public class ParentConfig {

}
